package com.avalon.client;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.avalon.client.resources.Resource;
import com.avalon.client.settings.SettingsManager;
import com.avalon.client.utilities.Saveable;
import com.google.common.collect.Lists;

import lombok.Getter;

public class Client {
	
	/**
	 * TODO: Check net.minecraft.gui.FontRenderer.java -> Fonts System (Settings)
	 * TODO: Try to add some Fonts, like Oswald, BubbleGum & PerfectlyTogether (TEST) 
	 * TODO: Try to has new Background().setTexture("").draw(); is sized to screen size auto.
	 */
	
	/**
	 * Instance
	 */
	
	@Getter private static Client instance;
	
	/**
	 * Logger
	 */
	
	@Getter public static Logger logger = LogManager.getLogger();
	
	/**
	 * Version
	 */
	
	public static String CLIENT_VERSION = "1.0.0-BETA";
	public static String SUB_VERSION = "1.0.0";
	public static String VERSION = "BETA";
	
	/**
	 * Saveables
	 */
	
	@Getter private static List<Saveable> saveables = Lists.newArrayList();
	
	public Client() {
		
		/**
		 * Instancing Client
		 */
		
		instance = this;
		
		/**
		 * Resource
		 */
		
		new Resource();
		
		/**
		 * Saveables
		 */
		
		saveables.add(new SettingsManager());
		
		for(Saveable saveable: saveables) { 
			saveable.load(); 
		}
	}
	
	public void save() {
		for(Saveable saveable: saveables) {
			saveable.save();
		}
	}

}
