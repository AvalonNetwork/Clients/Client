package com.avalon.client.interfaces.minecraft;

import java.awt.Color;

import com.avalon.client.components.Component;
import com.avalon.client.components.backgrounds.ColoredBackground;
import com.avalon.client.components.backgrounds.TexturedBackground;
import com.avalon.client.components.buttons.Button;
import com.avalon.client.components.buttons.actions.ConnectAction;
import com.avalon.client.components.buttons.actions.DisplayScreenAction;
import com.avalon.client.components.buttons.type.DefaultType;
import com.avalon.client.interfaces.Interface;
import com.avalon.client.resources.Resource;
import com.avalon.client.utilities.Colors;
import com.avalon.client.utilities.Utilities;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiSelectWorld;

public class HomeInterface extends Interface {
	
	@Getter @Setter
		private float backgroundTransparency = 0.01f;

	@Override
	public void initInterface() {
		
		// Rejoindre
		Button connect = new Button(this.getSplitWidth() - 50, this.getSplitHeight() - 20);
        connect.setText("Rejoindre");
        connect.setLightColor(new Color(2, 119, 189).getRGB());
        connect.setDarkColor(new Color(21, 101, 192).getRGB());
        connect.setSize(100, 30);
        connect.setType(new DefaultType());
        connect.addAction(new ConnectAction(this, "proxy.avalon-network.net:13080"));
        connect.setVisible(false);
        this.addComponent(connect);
        
		// Solo
		Button solo = new Button(this.getSplitWidth() - 50, this.getSplitHeight() + 15);
		solo.setText("Solo");
		solo.setLightColor(new Color(2, 119, 189).getRGB());
		solo.setDarkColor(new Color(21, 101, 192).getRGB());
		solo.setSize(100, 30);
		solo.setType(new DefaultType());
        solo.addAction(new DisplayScreenAction(new GuiSelectWorld(this)));
        solo.setVisible(false);
        this.addComponent(solo);
        
        
		// Options
		Button options = new Button(this.getSplitWidth() - 50, this.getSplitHeight() + 50);
		options.setText("Options");
		options.setLightColor(new Color(2, 119, 189).getRGB());
		options.setDarkColor(new Color(21, 101, 192).getRGB());
		options.setSize(100, 30);
		options.setType(new DefaultType());
        options.addAction(new DisplayScreenAction(new GuiOptions(this, Minecraft.getMinecraft().gameSettings)));
		options.setVisible(false);
        this.addComponent(options);
	}

	@Override
	public void updateInterface() {

		if(!(this.backgroundTransparency >= 0.5f)) 
     		this.backgroundTransparency += 0.01f;
		
		if(backgroundTransparency >= 0.5f) 
			for(Component component: getComponents()) 
				component.setVisible(true);
	}

	@Override
	public void drawComponents() {
		
		// Background 
    	TexturedBackground background = new TexturedBackground();
    	background.setTexture(Resource.get().getResource("main_background"));
    	background.draw();
    	
    	// Shadows
    	ColoredBackground shadow = new ColoredBackground();
    	shadow.setColor(Colors.BLUR);
    	shadow.setTransparency(backgroundTransparency);
    	shadow.draw();
    	
    	// Menu
    	ColoredBackground menu = new ColoredBackground(this.getSplitWidth() - 90, 0, 180, height);
    	menu.setColor(Colors.COBALT);
    	menu.setTransparency(backgroundTransparency);
    	menu.draw();
    	
    	if(backgroundTransparency >= 0.5f)
    		Utilities.displayLogo(150, 80, this.getSplitWidth() - 75, this.getSplitHeight() - 115);
	}

}
