package com.avalon.client.interfaces;

import com.avalon.client.components.backgrounds.ColoredBackground;
import com.avalon.client.components.backgrounds.TexturedBackground;
import com.avalon.client.components.buttons.Button;
import com.avalon.client.components.buttons.actions.ConnectAction;
import com.avalon.client.components.buttons.type.DefaultType;
import com.avalon.client.resources.Resource;
import com.avalon.client.utilities.Colors;

public class TestInterface extends Interface {

	@Override
	public void initInterface() {
        Button connect = new Button(100, this.getHeight() / 2);
        connect.setText("&7&lRejoindre");
        connect.setColor(Colors.COBALT);
        connect.setSize(200, 30);
        connect.setType(new DefaultType());
        connect.addAction(new ConnectAction(this, "54.36.122.15:25565"));
        this.addComponent("e", connect);
	}

	@Override
	public void updateInterface() {
		
		
	}

	@Override
	public void drawComponents() {
		
    	ColoredBackground cBG = new ColoredBackground();
    	cBG.setAnimated(true);
    	cBG.setColor(Colors.BLUR);
    	cBG.setTransparency(0.5f);
    	cBG.draw();
    	
    	TexturedBackground tBG = new TexturedBackground();
    	tBG.setTexture(Resource.get().getResource("main_background"));
    	tBG.draw();
    	
    	cBG.draw();
	}

}
