package com.avalon.client.components;

import org.lwjgl.opengl.GL11;

import com.avalon.client.utilities.Utilities;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;

@Getter @Setter
public class BlockComponent extends SizedComponent {
	
	private Integer lightColor;
	private Integer darkColor;
	private Float transparency = 1f;
	
	private boolean borders;

	public BlockComponent(Integer posX, Integer posY) {
		super(posX, posY);
	}

	@Override
	public void draw() {
        Tessellator tessellator = Tessellator.instance;
        
        GL11.glPushMatrix();
        GL11.glEnable((int)3042);
        GL11.glDisable((int)3553);
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        
        Utilities.setGlColor(this.lightColor, this.transparency);
        
        tessellator.startDrawingQuads();
        tessellator.addVertex(this.getPosX(), this.getPosY() + this.getHeight(), 0.0);
        tessellator.addVertex(this.getPosX() + this.getWidth(), this.getPosY() + this.getHeight(), 0.0);
        tessellator.addVertex(this.getPosX() + this.getWidth(), this.getPosY(), 0.0);
        tessellator.addVertex(this.getPosX(), this.getPosY(), 0.0);
        tessellator.draw();
        
        GL11.glColor4f((float)0.0f, (float)0.0f, (float)0.0f, (float)this.transparency);
        
        if (this.borders) {
            Integer color = Utilities.convertGlColor(this.getDarkColor(), this.transparency).getRGB();
            Utilities.displayGradientColor(this.getPosX() - 1, this.getPosY() - 1, this.getPosX(), this.getPosY() + this.getHeight() + 1, color, color);
            Utilities.displayGradientColor(this.getPosX() + this.getWidth(), this.getPosY(), this.getPosX() + this.getWidth() + 1, this.getPosY() + this.getHeight(), color, color);
            Utilities.displayGradientColor(this.getPosX(), this.getPosY() - 1, this.getPosX() + this.getWidth() + 1, this.getPosY(), color, color);
            Utilities.displayGradientColor(this.getPosX(), this.getPosY() + this.getHeight(), this.getPosX() + this.getWidth() + 1, this.getPosY() + this.getHeight() + 1, color, color);
        }
        
        GL11.glDisable((int)2929);
        GL11.glDisable((int)3008);
        GL11.glEnable((int)3042);
        GL11.glEnable((int)3553);
        GL11.glColor4f((float)1.0f, (float)1.0f, (float)1.0f, (float)1.0f);
        GL11.glPopMatrix();
	}

}
