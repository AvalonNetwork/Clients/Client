package com.avalon.client.components;

import com.avalon.client.utilities.Utilities;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public abstract class SizedComponent extends Component {
	
	private Integer width;
	private Integer height;	

	public SizedComponent(Integer posX, Integer posY) {
		super(posX, posY);
	}
	
    public boolean isHovered() {
        if (Utilities.getMouseX() >= this.getPosX() && Utilities.getMouseY() >= this.getPosY() && Utilities.getMouseX() < this.getPosX() + this.width && Utilities.getMouseY() < this.getPosY() + this.height) {
            return true;
        }
        return false;
    }

    public boolean isPressed() {
        if (this.isActive() && this.isHovered()) {
            return true;
        }
        return false;
    }
}
