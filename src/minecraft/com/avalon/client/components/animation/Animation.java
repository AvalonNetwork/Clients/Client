package com.avalon.client.components.animation;

import com.avalon.client.components.Component;

public interface Animation {

	public void update(Component component);
	
}
