package com.avalon.client.components.animation;

import com.avalon.client.Client;
import com.avalon.client.components.Component;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TranslationAnimation implements Animation {
	
	private boolean start = false;
	private boolean animated = false;
	private boolean finish = false;
	
	private Movement movement;
	
	private Integer startPosX;
	private Integer startPosY;
	
	private Integer speed = 1;
	
	private Integer endPoint;

	@Override
	public void update(Component component) {
		
		/**
		 * Start Animation 
		 */
		
		if(isFinish()) {
			Client.getLogger().info("[Animation] Animation finish.");
			return;
		}
		
		if(!isStart()) {
			start = true;
			animated = true;
			
			startPosX = component.getPosX();
			startPosY = component.getPosY();
			
			Client.getLogger().info("[Animation] Starting Animation of " + component.getClass().getName());
		}
		
		/**
		 * Animate @Component
		 */
		
		if(movement == Movement.RIGHT) {
			
			Integer position = component.getPosX() + speed;
			component.setPosX(position);
			
			Client.getLogger().info("[Animation] Animated Animation of " + component.getClass().getName());
		}
		
		
		
		
	}
	
	public enum Movement {
		
		LEFT,
		
		RIGHT,
		
		UP,
		
		DOWN;
	}

}
