package com.avalon.client.components;

import java.util.List;

import com.avalon.client.components.animation.Animation;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public abstract class Component {
	
	private String id;
	
	private Integer posX;
	private Integer posY;
	
	private boolean active;
	private boolean visible;
	private boolean selected;
	
	private List<Animation> animations = Lists.newArrayList();
	
	public Component(Integer posX, Integer posY) {
		this.posX = posX;
		this.posY = posY;
		
		this.active = true;
		this.visible = true;
		this.selected = false;
	}
	
	public abstract void draw();
	
	public void addAnimation(Animation animation) {
		this.animations.add(animation);
	}
}
