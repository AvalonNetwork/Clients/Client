package com.avalon.client.components.buttons.type;

import java.awt.Color;

import com.avalon.client.utilities.Colors;
import com.avalon.client.utilities.Utilities;

import lombok.Getter;

@Getter
public class EscapeType extends Type {
	
	private boolean init = false;
	private int updateCounter;
	private boolean alsoHovered = false;

	@Override
	public void display() {
		
		if(isInit()) 
			this.updateCounter = this.getButton().getPosX();
		
        if(this.getButton().isHovered()) {
        
        	this.alsoHovered = true;
    	   
        	if(!(this.updateCounter >= this.getButton().getWidth())) 
        		this.updateCounter += 1;
        	
    	   
        	Utilities.displayGradientColor(this.getButton().getPosX(), this.getButton().getPosY(), this.getButton().getPosX() + this.getButton().getWidth(), this.getButton().getPosY() + this.getButton().getHeight(), Colors.BLUR.getLightColor(), Colors.BLUR.getLightColor());
        	Utilities.displayGradientColor(this.getButton().getPosX(), this.getButton().getPosY() + this.getButton().getHeight() - 4, this.getButton().getPosX() + updateCounter, this.getButton().getPosY() + this.getButton().getHeight(), new Color(217,43,76).getRGB(), new Color(217,43,76).getRGB());
        }
        
        if(this.isAlsoHovered() && this.updateCounter >= 0 && !this.getButton().isHovered()) {
        	this.updateCounter -= 1;
        	Utilities.displayGradientColor(this.getButton().getPosX(), this.getButton().getPosY() + this.getButton().getHeight() - 4, this.getButton().getPosX() + updateCounter, this.getButton().getPosY() + this.getButton().getHeight(), new Color(217,43,76).getRGB(), new Color(217,43,76).getRGB());
    	}
        
		
	}

}
