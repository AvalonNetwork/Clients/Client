package com.avalon.client.components.buttons.type;

import com.avalon.client.components.buttons.Button;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter 
public abstract class Type {
	
    private Button button;

    public abstract void display();
    
}

