package com.avalon.client.components.buttons.type;

import com.avalon.client.components.BlockComponent;
import com.avalon.client.utilities.Colors;

public class DefaultType extends Type {

    @Override
    public void display() {
    	Integer color = this.getButton().isActive() ? (this.getButton().isHovered() ? this.getButton().getLightColor() : this.getButton().getDarkColor()) : Colors.LIGHT_GRAY.getLightColor();
    	
        BlockComponent rect = new BlockComponent(this.getButton().getPosX(), this.getButton().getPosY());
        rect.setLightColor(color);
        
        rect.setWidth(this.getButton().getWidth());
        rect.setHeight(this.getButton().getHeight());
        
        rect.draw();
    }

}
