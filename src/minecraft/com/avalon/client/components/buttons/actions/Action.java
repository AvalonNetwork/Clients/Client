package com.avalon.client.components.buttons.actions;

public abstract class Action {
	
	public abstract void execute();

}
