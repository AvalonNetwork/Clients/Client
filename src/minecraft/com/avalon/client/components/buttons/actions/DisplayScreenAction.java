package com.avalon.client.components.buttons.actions;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class DisplayScreenAction
extends Action {
    private GuiScreen target;

    public DisplayScreenAction(GuiScreen target) {
        this.target = target;
    }

    @Override
    public void execute() {
    	Minecraft.getMinecraft().displayGuiScreen(target);
    }
}

