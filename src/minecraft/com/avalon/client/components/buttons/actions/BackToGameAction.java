package com.avalon.client.components.buttons.actions;

import net.minecraft.client.Minecraft;

public class BackToGameAction extends Action {

	@Override
	public void execute() {
		
		Minecraft.getMinecraft().displayGuiScreen(null);
		Minecraft.getMinecraft().setIngameFocus();
		
	}

}
