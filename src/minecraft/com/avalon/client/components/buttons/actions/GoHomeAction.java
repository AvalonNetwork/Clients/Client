package com.avalon.client.components.buttons.actions;

import com.avalon.client.interfaces.minecraft.HomeInterface;

import net.minecraft.client.Minecraft;

public class GoHomeAction extends Action {

	@Override
	public void execute() {
		
		Minecraft.getMinecraft().theWorld.sendQuittingDisconnectingPacket();
		Minecraft.getMinecraft().loadWorld(null);
		Minecraft.getMinecraft().displayGuiScreen(new HomeInterface());
		
	}

}
