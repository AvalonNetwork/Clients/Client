package com.avalon.client.components.buttons.actions;

import com.avalon.client.interfaces.Interface;
import com.ibm.icu.impl.duration.impl.Utils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.multiplayer.ServerData;

public class ConnectAction extends Action  {

    private Interface current;
    private String adress;

    public ConnectAction(Interface current, String adress) {
        this.current = current;
        this.adress = adress;
    }

    @Override
    public void execute() {
        Minecraft.getMinecraft().displayGuiScreen(new GuiConnecting(this.current, Minecraft.getMinecraft(), new ServerData("Avalon", this.adress)));
    }

}
