package com.avalon.client.components.buttons;

import java.util.List;

import com.avalon.client.components.SizedComponent;
import com.avalon.client.components.buttons.actions.Action;
import com.avalon.client.components.buttons.informations.DefaultInformations;
import com.avalon.client.components.buttons.informations.Informations;
import com.avalon.client.components.buttons.type.DefaultType;
import com.avalon.client.components.buttons.type.Type;
import com.avalon.client.utilities.Colors;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Button extends SizedComponent {
	
	private String text;
	
	private Type type;
	private Informations information;
	private List<Action> actions;
	
	private Integer lightColor;
	private Integer darkColor;

	public Button(Integer posX, Integer posY) {
		super(posX, posY);
		
		this.actions = Lists.newArrayList();
		this.setInformation(new DefaultInformations());
		this.setType(new DefaultType());
		this.setColor(Colors.GRAY);
		
		this.setWidth(150);
		this.setHeight(30);
	}
	
	public void setType(Type type) {
		this.type = type;
		this.type.setButton(this);
	}
	
	public void setInformation(Informations informations) {
		this.information = informations;
		this.information.setButton(this);
	}
	
	public void addAction(Action action) {
		this.actions.add(action);
	}
	
	public void setColor(Colors color) {
		this.lightColor = color.getLightColor();
		this.darkColor  = color.getDarkColor();
	}
	
	public void setSize(Integer width, Integer height) {
		this.setWidth(width);
		this.setHeight(height);
	}

	public void draw() {
		if (this.type != null) 
			 this.type.display();
		
		if(this.information != null)
			this.information.display();
	}
}
