package com.avalon.client.components.buttons.informations;

import com.avalon.client.utilities.Utilities;

public class DefaultInformations extends Informations {

	@Override
	public void display() {
		String title = this.getButton().getText();
		
        if (title != null) {
            Utilities.displayString(title, this.getButton().getPosX() + (this.getButton().getWidth() - Utilities.getStringWidth(title)) / 2, this.getButton().getPosY() + (this.getButton().getHeight() - 8) / 2);
        }
		
	}

}
