package com.avalon.client.components.buttons.informations;

import com.avalon.client.utilities.Utilities;

public class LeftPosition extends Informations  {

	@Override
	public void display() {
		String title = this.getButton().getText();
		
        if (title != null) {
            Utilities.displayString(title, this.getButton().getPosX() + 5, this.getButton().getPosY() + (this.getButton().getHeight() - 8) / 2);
        }
		
	}

}
