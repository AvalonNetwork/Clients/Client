package com.avalon.client.components.buttons.informations;

import com.avalon.client.components.buttons.Button;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public abstract class Informations {

	private Button button;
	
	public abstract void display();
	
}
