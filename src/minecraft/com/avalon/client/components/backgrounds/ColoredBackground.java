package com.avalon.client.components.backgrounds;

import com.avalon.client.components.BlockComponent;
import com.avalon.client.utilities.Colors;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ColoredBackground extends Background {

	/**
	 * Color
	 */
	
	private Integer lightColor; 
	private Integer darkColor;
	
	private boolean borders = false;

	/**
	 * Animation
	 */
	
	private boolean animated = false;
	
	private float start = 0.1f;
	private float speed = 0.02f;
	private float max = 1.0f;
	
	private float transparency = start;
	
	public ColoredBackground() {
		super();
	}
	
	public ColoredBackground(Integer posX, Integer posY, Integer width, Integer height) {
		super(posX, posY, width, height);
	}

	@Override
	public void draw() {
		
		BlockComponent rect = new BlockComponent(this.getPosX(), this.getPosY());
		
		rect.setLightColor(lightColor);
		rect.setDarkColor(darkColor);
		rect.setBorders(borders);
		
		rect.setTransparency(transparency);
		
		rect.setWidth(this.getWidth());
		rect.setHeight(this.getHeight());
		
		rect.draw();
	}
	
	/**
	 * Colors Methods
	 */
	
	public void setColor(Colors color) {
		this.lightColor = color.getLightColor();
		this.darkColor  = color.getDarkColor();
	}

}
