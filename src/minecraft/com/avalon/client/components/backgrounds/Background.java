package com.avalon.client.components.backgrounds;

import com.avalon.client.components.SizedComponent;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

@Getter @Setter
public abstract class Background extends SizedComponent {
	
	private Integer width;
	private Integer height;
	
	public Background() {
		super(0, 0);
		
		this.width = GuiScreen.getWidth();
		this.height = GuiScreen.getHeight();
	}
	
	public Background(Integer posX, Integer posY, Integer width, Integer height) {
		super(posX, posY);
		
		this.width = width;
		this.height = height;
	}

	public abstract void draw();
}
