package com.avalon.client.components.backgrounds;

import org.lwjgl.opengl.GL11;

import com.avalon.client.utilities.Utilities;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;

@Getter @Setter
public class TexturedBackground extends Background {
	
	private ResourceLocation texture;
	
	public TexturedBackground() {
		super();
	}
	
	public TexturedBackground(Integer posX, Integer posY, Integer width, Integer height) {
		super(posX, posY, width, height);
	}

	@Override
	public void draw() {
		if(texture == null)
			return;
        
        GL11.glEnable((int)3008);
        GL11.glEnable((int)3042);
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        
        Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
        
        Utilities.displayImageSized(0, 0, 0, 0, this.getWidth(), this.getHeight());
        
        GL11.glDisable((int)3042);
        GL11.glDisable((int)3008);
	}
}
