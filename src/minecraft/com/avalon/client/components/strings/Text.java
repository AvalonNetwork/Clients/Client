package com.avalon.client.components.strings;

import org.lwjgl.opengl.GL11;

import com.avalon.client.components.Component;
import com.avalon.client.utilities.Colors;
import com.avalon.client.utilities.Utilities;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Text extends Component {
	
    private String title;
    private TextSize size;
    private Integer color;

	public Text(String title, Integer posX, Integer posY) {
		super(posX, posY);
        this.title = title;
        this.size = TextSize.DEFAULT;
        this.color = 16777215;
	}

	@Override
	public void draw() {
        float finalSize = this.size.getSize();
        GL11.glPushMatrix();
        GL11.glScalef((float)finalSize, (float)finalSize, (float)finalSize);
        Utilities.displayString(this.title, (int)((float)this.getPosX() / finalSize), (int)((float)this.getPosY() / finalSize), color);
        GL11.glPopMatrix();
	}
}
