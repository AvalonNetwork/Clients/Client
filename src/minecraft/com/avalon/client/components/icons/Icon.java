package com.avalon.client.components.icons;

import com.avalon.client.components.SizedComponent;
import com.avalon.client.resources.Resource;

import net.minecraft.client.Minecraft;

public class Icon extends SizedComponent {

	public Icon(Integer posX, Integer posY) {
		super(posX, posY);
		
		this.setWidth(10);
		this.setHeight(10);
	}

	@Override
	public void draw() {
		Minecraft.getMinecraft().getTextureManager().bindTexture(Resource.get().getResource("cursor"));
	}

}
