package com.avalon.client.settings;

import java.io.File;

import com.avalon.client.utilities.DiscUtilities;
import com.avalon.client.utilities.Saveable;
import com.google.common.reflect.TypeToken;

import lombok.Getter;
import net.minecraft.client.Minecraft;

public class SettingsManager extends Saveable {
	
	/**
	 * Instance
	 */
	
	@Getter private static SettingsManager instance;
	
	/**
	 * Settings
	 */
	
	private Settings settings;
	
	/**
	 * @Constructor
	 * 
	 *   Instancing this class to be a static class.
	 *   Create Settings Folder
	 *   
	 */
	
	public SettingsManager() {
		instance = this;
		
		new File(Minecraft.getMinecraft().mcDataDir + "/settings/").mkdirs();
	}
	
	/**
	 * Return user settings
	 * 
	 * @return
	 * 		Settings
	 * 
	 */
	
	public Settings getSettings() {
		if(this.settings == null) { return settings = new Settings(); }
		
		return this.settings;
	}
	
	/******************************
	 * 
	 * 		Abstract Methods
	 * 
	 ******************************/
	
	@Getter private File File = new File(Minecraft.getMinecraft().mcDataDir + "/settings/settings.json");
	
	/**
	 * Loading.
	 */
	
	@Override
	public void load() {
		String content = DiscUtilities.readCatch(this.getFile());
		
		if(content == null) return;
		
		this.settings = this.gsonInstance().fromJson(content, new TypeToken<Settings>(){}.getType());
	}
	
	/**
	 * Saving.
	 */
	
	@Override
	public void save() {
		DiscUtilities.writeCatch(this.getFile(), this.gsonInstance().toJson(this.getSettings()));
	}

}
