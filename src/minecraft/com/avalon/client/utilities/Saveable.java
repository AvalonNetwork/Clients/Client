package com.avalon.client.utilities;

import java.io.File;
import java.lang.reflect.Modifier;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class Saveable {
	
	/**
	 * Loading File into an Object
	 */
	
	public abstract void load(); 
	
	/**
	 * Return a File
	 * 
	 * @return
	 * 		File to return
	 * 
	 */
	
	public abstract File getFile();
	
	/**
	 * Save Object to File
	 */
	
	public abstract void save(); 
	
	/**
	 * Return GSON Libraries
	 * 
	 * @return
	 * 		Gson instance
	 * 
	 */
	
	public Gson gsonInstance() {
		return new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().serializeNulls().excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE).create();
	}

}
