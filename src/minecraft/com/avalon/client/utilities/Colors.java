package com.avalon.client.utilities;

import java.awt.Color;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
public enum Colors {
	
    LIGHT_RED(new Color(231, 76, 60), new Color(203, 67, 53)),
    DARK_RED(new Color(192, 57, 43), new Color(169, 50, 38)),
    RUBY_RED(new Color(248, 57, 66), new Color(237, 75, 81)),
    FRUIT_RED(new Color(237, 85, 101), new Color(218, 68, 83)),
    MINT_GREEN(new Color(72, 207, 173), new Color(55, 188, 155)),
    GREEN(new Color(160, 212, 104), new Color(140, 193, 82)),
    LIGHT_GREEN(new Color(46, 204, 113), new Color(40, 180, 99)),
    DARK_GREEN(new Color(39, 174, 96), new Color(34, 153, 84)),
    LIGHT_MIDNIGHT(new Color(86, 101, 115), new Color(44, 62, 80)),
    DARK_MIDNIGHT(new Color(93, 109, 126), new Color(52, 73, 94)),
    MIDNIGHT(new Color(56, 56, 56), new Color(34, 32, 43)),
    ORANGE(new Color(252, 110, 81), new Color(233, 87, 63)),
    YELLOW(new Color(244, 208, 63), new Color(241, 196, 15)),
    LIGHT_YELLOW(new Color(255, 206, 84), new Color(246, 187, 66)),
    BLUE(new Color(93, 156, 236), new Color(74, 137, 220)),
    LIGHT_BLUE(new Color(79, 193, 233), new Color(59, 175, 218)),
    PURPLE(new Color(155, 89, 182), new Color(142, 68, 173)),
    DARK_PURPLE(new Color(46, 38, 48), new Color(42, 36, 44)),
    CLOUD(new Color(249, 249, 249), new Color(228, 228, 228)),
    LIGHT_GRAY(new Color(204, 209, 217), new Color(170, 178, 189)),
    GRAY(new Color(47, 47, 47), new Color(41, 41, 41)),
    PINK(new Color(236, 135, 192), new Color(215, 112, 173)),
    BROWN(new Color(210, 192, 170), new Color(202, 180, 148)),
    LIGHT_BROWN(new Color(232, 206, 177), new Color(193, 165, 132)),
    BLACK(new Color(48, 48, 48), new Color(27, 26, 26)),
    BEIGE(new Color(254, 254, 254), new Color(207, 186, 165)),
    WHITE(new Color(253, 254, 254), new Color(236, 240, 241)),
	
	DEEP(new Color(103, 58, 183),  new Color(81, 45, 168)),
	COBALT(new Color(76,98,131), new Color(54, 67, 92)),
	BLUR(new Color(12, 25, 52), new Color(8, 3, 37));
    
    private int lightColor, darkColor;

	Colors(Color light, Color dark) {
		this.lightColor = light.getRGB();
		this.darkColor  = dark.getRGB();
	}

}
