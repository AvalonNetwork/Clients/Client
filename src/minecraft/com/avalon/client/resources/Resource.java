package com.avalon.client.resources;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.util.ResourceLocation;

public class Resource {

	/**
	 * Instance
	 */
	
	private static Resource instance;
	
	/**
	 * Map
	 */
	
	private Map<String, ResourceLocation> resources = new HashMap<String, ResourceLocation>();
	
	/**
	 * @Constructor 
	 * 
	 *  Instancing this class to be static.
	 *   Register all resources.
	 *   
	 */
	
	public Resource() {
		instance = this;
		
		this.register();
	}
	
	/**
	 * Return instance
	 * 
	 * @return
	 * 		Resource instance
	 * 
	 */
	
	public static Resource get() {
		return instance;
	}

	private void register() {
		
		/**
		 * Interface 
		 */
		
		this.addResource("logo", Ressource.INTERFACE);
		this.addResource("background", Ressource.INTERFACE);
			
		this.addResource("main_background", Ressource.MAIN_INTERFACE);
		
		this.addResource("close", Ressource.ICON);
	}
	
	/**
	 * Loading some Resource
	 * 
	 * @param 
	 * 		name to give to resource
	 * 
	 * @param 
	 * 		ressource type
	 * 
	 */
	
    public void addResource(String name, Ressource ressource) {
        this.resources.put(name, new ResourceLocation(String.valueOf(ressource.getPath()) + name + ".png"));
    }
	
	/**
	 * Return an resource
	 * 
	 * @param 	
	 * 		name of the resource to return.
	 * 
	 * @return
	 * 		Resource
	 * 
	 */
	
	public ResourceLocation getResource(String name) {
		return this.resources.get(name);
	}
	
	@Getter @AllArgsConstructor
	public enum Ressource {
		
	    INTERFACE("textures/resources/interface/"),
	    	MAIN_INTERFACE("textures/resources/interface/main/"),
	    
		ICON("textures/resources/icon/");
	    
	    @Getter private String path;
	}
}
