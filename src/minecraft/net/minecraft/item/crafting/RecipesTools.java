package net.minecraft.item.crafting;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class RecipesTools
{
    private String[][] recipePatterns = new String[][] {{"XXX", " # ", " # "}, {"X", "#", "#"}, {"XX", "X#", " #"}, {"XX", " #", " #"}};
    private Object[][] recipeItems;
    private static final String __OBFID = "CL_00000096";

    public RecipesTools()
    {
        this.recipeItems = new Object[][] {{Blocks.planks, Blocks.cobblestone, Items.iron_ingot, Items.diamond, Items.gold_ingot, Items.platine, Items.realgar, Items.rosalys, Items.celestium, Items.comete},
        	{Items.wooden_pickaxe, Items.stone_pickaxe, Items.iron_pickaxe, Items.diamond_pickaxe, Items.golden_pickaxe, Items.platine_pickaxe, Items.realgar_pickaxe, Items.rosalys_pickaxe, Items.celestium_pickaxe, Items.comete_pickaxe}, 
        	{Items.wooden_shovel, Items.stone_shovel, Items.iron_shovel, Items.diamond_shovel, Items.golden_shovel, Items.platine_shovel, Items.realgar_shovel, Items.rosalys_shovel, Items.celestium_shovel, Items.comete_shovel}, 
        	{Items.wooden_axe, Items.stone_axe, Items.iron_axe, Items.diamond_axe, Items.golden_axe, Items.platine_axe, Items.realgar_axe, Items.rosalys_axe, Items.celestium_axe, Items.comete_axe}, 
        	{Items.wooden_hoe, Items.stone_hoe, Items.iron_hoe, Items.diamond_hoe, Items.golden_hoe, Items.platine_hoe, Items.realgar_hoe, Items.rosalys_hoe, Items.celestium_hoe, Items.diamond_hoe}};
    }
    public void addRecipes(CraftingManager p_77586_1_) {
        int var2 = 0;
        while (var2 < this.recipeItems[0].length) {
            Object var3 = this.recipeItems[0][var2];
            int var4 = 0;
            while (var4 < this.recipeItems.length - 1) {
                Item var5 = (Item)this.recipeItems[var4 + 1][var2];
                p_77586_1_.addRecipe(new ItemStack(var5), this.recipePatterns[var4], Character.valueOf('#'), Items.stick, Character.valueOf('X'), var3);
                ++var4;
            }
            ++var2;
        }
        p_77586_1_.addRecipe(new ItemStack(Items.shears), " #", "# ", Character.valueOf('#'), Items.iron_ingot);
    }
}
