package net.minecraft.item;

public enum Rarity {
	
	UNCOMMON,
	
	COMMON, 
	
	RARE,
	
	EPIC,
	
	LEGENDARY;
}
