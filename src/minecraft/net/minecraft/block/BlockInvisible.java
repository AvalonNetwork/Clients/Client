package net.minecraft.block;

import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockStone;
import net.minecraft.block.material.MapColor;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;

public class BlockInvisible
extends BlockStone {
    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public Item getItemDropped(int count, Random random, int value) {
        return Item.getItemFromBlock(Blocks.invisible);
    }

    @Override
    public MapColor getMapColor(int color) {
        return MapColor.obsidianColor;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }
}

