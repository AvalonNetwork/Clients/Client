package net.minecraft.src.json;

public interface JSONAware
{
    String toJSONString();
}
