package net.minecraft.client.gui;

import com.avalon.client.components.backgrounds.ColoredBackground;
import com.avalon.client.components.buttons.Button;
import com.avalon.client.components.buttons.actions.BackToGameAction;
import com.avalon.client.components.buttons.actions.DisplayScreenAction;
import com.avalon.client.components.buttons.actions.GoHomeAction;
import com.avalon.client.components.buttons.informations.LeftPosition;
import com.avalon.client.components.buttons.type.EscapeType;
import com.avalon.client.interfaces.Interface;
import com.avalon.client.utilities.Colors;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.Minecraft;

public class GuiIngameMenu extends Interface {
	
	@Getter @Setter
	private float backgroundTransparency = 0.01f;

	@Override
	public void initInterface() {
		
		// Options
		Button options = new Button(0, this.getSplitHeight() - 80);
		options.setText("OPTIONS");
		options.setColor(Colors.GRAY);
		options.setSize(135, 25);
		options.setInformation(new LeftPosition());
		options.setType(new EscapeType());
		options.addAction(new DisplayScreenAction(new GuiOptions(this, Minecraft.getMinecraft().gameSettings)));
        this.addComponent(options);

		// Options - Avalon
		Button avalonOptions = new Button(0, this.getSplitHeight() - 55);
		avalonOptions.setText("OPTIONS AVALON");
		avalonOptions.setColor(Colors.GRAY);
		avalonOptions.setSize(135, 25);
		avalonOptions.setInformation(new LeftPosition());
		avalonOptions.setType(new EscapeType());
        this.addComponent(avalonOptions);
        
		Button back = new Button(0, this.getSplitHeight() - 30);
		back.setText("RETOUR EN JEU");
		back.setColor(Colors.GRAY);
		back.setSize(135, 25);
		back.setInformation(new LeftPosition());
		back.setType(new EscapeType());
		back.addAction(new BackToGameAction());
        this.addComponent(back);
        
		Button quit = new Button(0, this.getSplitHeight() - 5);
		quit.setText("RETOUR AU MENU");
		quit.setColor(Colors.GRAY);
		quit.setSize(135, 25);
		quit.setInformation(new LeftPosition());
		quit.setType(new EscapeType());
		quit.addAction(new GoHomeAction());
        this.addComponent(quit);
        
	}

	@Override
	public void updateInterface() {
		
		if(!(this.backgroundTransparency >= 0.5f)) 
     		this.backgroundTransparency += 0.05f;
		
	}

	@Override
	public void drawComponents() {
		
		// Menu
		ColoredBackground menu = new ColoredBackground(0, 0, 135, height);
		menu.setColor(Colors.BLUR);
		menu.setTransparency(1);
		menu.draw();
		
		// Background
		ColoredBackground background = new ColoredBackground(135, 0, width, height);
		background.setColor(Colors.BLUR);
		background.setTransparency(backgroundTransparency);
		background.draw();
		
		
		
	}
}
